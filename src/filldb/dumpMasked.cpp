#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "json.hpp"

using namespace std;
using json = nlohmann::json;

static void dumpMaskedData(json &data)
{
    // table
    vector<vector<float>> content = data["hist"]["bins"]["content"].get<vector<vector<float>>>();
    const string title = data["hist"]["title"];
    int iz = 0;
    if (title.find("EE +") != string::npos)
        iz = 1;
    if (title.find("EE -") != string::npos)
        iz = -1;
    int rowid = data["hist"]["yaxis"]["first"]["value"].get<int>();
    for (auto &row : content)
    {
        rowid++;
        int colid = data["hist"]["xaxis"]["first"]["value"].get<int>();
        for (auto &col : row)
        {
            colid++;
            if (col == 0)
                continue;
            // status 0|1|-1 xaxis yaxis
            cout << col << " " << iz << " " << colid << " " << rowid << endl;
        }
    }
}

int main(int argc, char **argv)
{
    vector<string> files;
    for (int i = 1; i < argc; ++i)
    {
        files.push_back(argv[i]);
    }
    for (auto &f : files)
    {
        ifstream _f(f);
        json data;
        _f >> data;
        dumpMaskedData(data);
    }
}