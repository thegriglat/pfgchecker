#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <map>

#include "json.hpp"
#include "TFile.h"
#include "TTree.h"

using json = nlohmann::json;
using namespace std;

struct ChannelCoord
{
    int ix;
    int iy;
    int iz;
    friend bool operator<(const ChannelCoord &a, const ChannelCoord &b)
    {
        return (a.ix < b.ix) ||
               (a.ix == b.ix && a.iy < b.iy) ||
               (a.ix == b.ix && a.iy == b.iy && a.iz < b.iz);
    }
};

struct Data
{
    // pedestal
    float pedestal_mean = 0;
    // pedestal rms
    float pedestal_rms = 0;
    // timing
    float timing_mean = 0;
    // timing rms
    float timing_rms = 0;
    // rec hit energy
    float rechit_energy = 0;
    // digi occupancy
    float digi_occupancy = 0;
};

static map<ChannelCoord, Data> channelData;

static bool has(string text, string pattern)
{
    return text.find(pattern) != string::npos;
}

static float *setValue(string key, Data &data)
{

    if (has(key, "pedestal rms"))
    {
        return &(data.pedestal_rms);
    }
    if (has(key, "pedestal"))
    {
        return &(data.pedestal_mean);
    }
    if (has(key, "timing rms"))
    {
        return &(data.timing_rms);
    }
    if (has(key, "timing"))
    {
        return &(data.timing_mean);
    }
    if (has(key, "rec hit energy"))
    {
        return &(data.rechit_energy);
    }
    if (has(key, "digi occupancy"))
    {
        return &(data.digi_occupancy);
    }
    return nullptr;
}

static int *setAxis(string key, ChannelCoord &channel)
{
    if (has(key, "ix"))
    {
        return &(channel.ix);
    }
    if (has(key, "iy"))
    {
        return &(channel.iy);
    }
    if (has(key, "iphi"))
    {
        return &(channel.ix);
    }
    if (has(key, "ieta"))
    {
        return &(channel.iy);
    }
    return nullptr;
}

static int zSign(string title)
{
    if (has(title, "EE+"))
        return 1;
    if (has(title, "EE-"))
        return -1;
    return 0;
}

static void processJSONData(json &j)
{
    const json hist = j["hist"];
    const string title = hist["title"].get<string>();
    const json xaxis = hist["xaxis"];
    const json yaxis = hist["yaxis"];
    const string xvar = xaxis["title"].get<string>();
    const string yvar = yaxis["title"].get<string>();

    // reset unused vars
    ChannelCoord channel;
    channel.ix = -999;
    channel.iy = -999;
    channel.iz = zSign(title);
    int xsign = 0;
    int ysign = 0;

    // is needed?
    Data data;
    int *x = setAxis(xvar, channel);
    int *y = setAxis(yvar, channel);
    if (!x)
    {
        cout << "Cannot find X axis: " << xvar << endl;
    };
    if (!y)
    {
        cout << "Cannot find Y axis: " << yvar << endl;
    }
    // set value
    float *value = setValue(title, data);
    if (!value)
    {
        cout << "Cannot find title!!! " << title << endl;
    }
    // set iz

    int x_start = xaxis["first"]["value"].get<int>();
    int x_end = xaxis["last"]["value"].get<int>();
    int y_start = yaxis["first"]["value"].get<int>();
    int y_end = yaxis["last"]["value"].get<int>();

    if (channel.iz != 0)
    {
        // ee
        xsign = has(xvar, "-") ? -1 : 1;
        ysign = has(yvar, "-") ? -1 : 1;
    }
    else
    {
        // EB
        xsign = has(xvar, "-") ? -1 : 1;
        ysign = (y_start < 0 || y_end < 0) ? -1 : 1;
    }
    if (xsign > 0)
    {
        x_start += xsign;
        x_end += xsign;
    }
    if (ysign > 0)
    {
        y_start += ysign;
        y_end += ysign;
    }
    int x_step = (x_end - x_start) / (xaxis["last"]["id"].get<int>() - xaxis["first"]["id"].get<int>() + 1);
    int y_step = (y_end - y_start) / (yaxis["last"]["id"].get<int>() - yaxis["first"]["id"].get<int>() + 1);

    vector<vector<float>> content = hist["bins"]["content"].get<vector<vector<float>>>();
    *x = x_start * xsign;
    *y = y_start * ysign;
    for (const auto &row : content)
    {
        *x = x_start;
        for (const auto &elem : row)
        {
            *value = elem;
            // skip empty EE bins
            if (*value != 0)
            {
                // find channel
                auto pos = channelData.find(channel);
                if (pos == channelData.end())
                {
                    // not found
                    channelData.insert(make_pair(channel, data));
                }
                else
                {
                    if (value == &(data.pedestal_mean))
                        pos->second.pedestal_mean = *value;
                    if (value == &(data.pedestal_rms))
                        pos->second.pedestal_rms = *value;
                    if (value == &(data.timing_mean))
                        pos->second.timing_mean = *value;
                    if (value == &(data.timing_rms))
                        pos->second.timing_rms = *value;
                    if (value == &(data.rechit_energy))
                        pos->second.rechit_energy = *value;
                    if (value == &(data.digi_occupancy))
                        pos->second.digi_occupancy = *value;
                }
                /*
                cout << "\tix/iphi=" << channel.ix;
                cout << "\tiy/ieta=" << channel.iy;
                cout << "\tiz=" << channel.iz;
                cout << "\tpedestal_mean=" << data.pedestal_mean;
                cout << "\tpedestal_rms=" << data.pedestal_rms;
                cout << "\ttiming_mean=" << data.timing_mean;
                cout << "\ttiming_rms=" << data.timing_rms;
                cout << "\trechit_energy=" << data.rechit_energy;
                cout << endl;
                */
            }
            *x += x_step * xsign;
        }
        *y += y_step * ysign;
    }
}

static void help()
{
    cout << "Usage: runnumber outputfile [jsonfiles jsonfiles ...]" << endl;
}

int main(int argn, char **argc)
{
    vector<string> args;
    args.reserve(argn);
    for (int i = 0; i < argn; ++i)
    {
        string s(argc[i]);
        args.push_back(s);
    }
    if (std::find(args.begin(), args.end(), "-h") != args.end())
    {
        help();
        return 0;
    }

    if (args.size() < 3)
    {
        help();
        return 0;
    }
    int run = std::stoi(args[1]);
    const string outfile = args[2];
    for (size_t fidx = 3; fidx < args.size(); ++fidx)
    {
        int pcent = (fidx - 3) * 100. / (args.size() - 3);
        const string filename = args[fidx];
        cout << "[" << pcent << "%] Process file '" << filename << "'..." << endl;
        ifstream _f(filename);
        json data;
        _f >> data;
        processJSONData(data);
        _f.close();
    }
    cout << "Finished." << endl
         << "Fill ROOT Tree...\t";
    TFile *rfile = new TFile(outfile.c_str(), "recreate");
    TTree *tree = new TTree("channelInfo", "channelInfo");
    ChannelCoord channel;
    Data data;
    tree->Branch("run", &run);
    tree->Branch("ix_iphi", &(channel.ix));
    tree->Branch("iy_ieta", &(channel.iy));
    tree->Branch("iz", &(channel.iz));
    tree->Branch("pedestal_mean", &(data.pedestal_mean));
    tree->Branch("pedestal_rms", &(data.pedestal_rms));
    tree->Branch("timing_mean", &(data.timing_mean));
    tree->Branch("timing_rms", &(data.timing_rms));
    tree->Branch("rechit_energy", &(data.rechit_energy));
    tree->Branch("digi_occupancy", &(data.digi_occupancy));
    for (auto &elem : channelData)
    {
        channel = elem.first;
        data = elem.second;
        tree->Fill();
    }
    rfile->Write();
    rfile->Close();
    cout << "Done." << endl;
    return 0;
}