#include <vector>
#include <iostream>
#include <string>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TMultiGraph.h"
#include "TGraph.h"
#include "THStack.h"
#include "TChain.h"
#include "TLegend.h"
#include "TLegendEntry.h"

using namespace std;

TCanvas *c = nullptr;
THStack *hh = nullptr;
TLegend *legend = nullptr;

static TH1F *getPedestalHist(char *key, int x, int y, int iz, vector<int> &runs)
{
    float pedestal;
    int irun;
    TChain tree("channelInfo");
    for (auto &r : runs)
    {
        tree.Add((dir + "/" + to_string(r) + ".root").c_str());
    }
    tree.SetBranchStatus("*", 0);
    tree.SetBranchStatus("run", 1);
    tree.SetBranchStatus(key, 1);
    tree.SetBranchStatus("ix_iphi", 1);
    tree.SetBranchStatus("iy_ieta", 1);
    tree.SetBranchStatus("iz", 1);

    string hname = "htmp_" + to_string(x) + "_" + to_string(y); //+ "(" + to_string(runsize) + ", 1, " + to_string(runsize + 1) + ")";
    // string filter = "iz == " + to_string(iz) + " && ix_iphi == " + to_string(x) + " && iy_ieta == " + to_string(y);
    string filter;
    if (iz == 0)
        filter = "iz == 0 && ix_iphi == " + to_string(x) + " && iy_ieta == " + to_string(y);
    else if (iz == 1)
        filter = "iz == 1 && ix_iphi == " + to_string(x) + " && iy_ieta == " + to_string(y);
    else if (iz == -1)
        filter = "iz == -1 && ix_iphi == " + to_string(x) + " && iy_ieta == " + to_string(y);

    TTree *subtree = tree.CopyTree(filter.c_str());
    // string what = "pedestal_mean:run >> " + hname;
    subtree->SetBranchStatus("*", 0);
    subtree->SetBranchStatus(key, 1);
    subtree->SetBranchStatus("run", 1);
    subtree->SetBranchAddress("run", &irun);
    subtree->SetBranchAddress(key, &pedestal);
    TH1F *h = new TH1F(hname.c_str(), hname.c_str(), runs.size(), 1, runs.size() + 1);
    map<int, float> values;
    for (int event = 0; event < subtree->GetEntriesFast(); ++event)
    {
        subtree->GetEntry(event);
        values[irun] = pedestal;
    }
    /* debug: print values
    for (auto &e : values)
    {
        cout << "run = " << e.first << "\t" << key << " = " << e.second << endl;
    }
    */
    for (unsigned int i = 0; i < runs.size(); ++i)
    {
        h->SetBinContent(i + 1, values[runs[i]]);
        h->GetXaxis()->SetBinLabel(i + 1, to_string(runs[i]).c_str());
    }
    return h;
}

string getTitle(char *key, int xmin, int xmax, int ymin, int ymax, int iz)
{
    string xaxis = (iz == 0) ? "iphi" : "ix";
    string yaxis = (iz == 0) ? "ieta" : "iy";
    string t = key;
    t += (iz == 0) ? " EB: " : ((iz > 0) ? " EE+: " : " EE-: ");

    t += xaxis + "=";
    if (xmin == xmax)
    {
        t += std::to_string(xmax);
    }
    else
    {
        t += std::to_string(xmin) + ".." + std::to_string(xmax);
    }
    t += ", " + yaxis + "=";
    if (ymin == ymax)
    {
        t += std::to_string(ymax);
    }
    else
    {
        t += std::to_string(ymin) + ".." + std::to_string(ymax);
    }
    return t;
}

void trend(char *key, int xmin, int xmax, int ymin, int ymax, int iz, const EColor color = kBlue, const string opts = "")
{
    if (!c || opts.length() == 0)
    {
        if (c)
            delete c;
        if (hh)
            delete hh;
        if (legend)
            delete legend;
        c = new TCanvas("trendc", "trendc", 800, 600);
        hh = new THStack();
        legend = new TLegend(0.7, 0.1, 0.9, 0.2);
    }
    c->cd();
    // one fed
    float perc = 0.;
    string title = getTitle(key, xmin, xmax, ymin, ymax, iz);
    for (int x = xmin; x <= xmax; ++x)
    {
        for (int y = ymin; y <= ymax; ++y)
        {
            cout << std::setprecision(3) << perc << "%\tchannel[" << x << ", " << y << "]" << endl;
            TH1F *hist = getPedestalHist(key, x, y, iz, runs);
            hist->SetLineColor(color);
            hist->Draw("A");
            hh->Add(hist);
            perc += (float)100. / ((xmax - xmin + 1) * (ymax - ymin + 1));
        }
    }
    auto item = legend->AddEntry(hh, title.c_str(), "l");
    item->SetLineColor(color);
    hh->Draw("L nostack");
    legend->Draw(opts.c_str());
    c->Draw(opts.c_str());
}

void trend(char *key, int x, int y, int iz, const EColor color = kBlue, const string opts = "")
{
    trend(key, x, x, y, y, iz, color, opts);
}